<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Добавить запись в блог</title>
</head>
<body>
<form:form method="POST" action="/add" modelAttribute="blog">
    <form:hidden path="id" />
    <p>Name:</p>
    <form:input path="name" />
	<p>Thumbnail:</p>
    <form:input path="thumbnail" />
    <form:input path="content" type="textarea"/>
    <input type="submit" />
</form:form>
</body>
</html>
