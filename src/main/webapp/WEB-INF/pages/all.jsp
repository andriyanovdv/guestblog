<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Все записи блога</title>
	<style>
	
	</style>
</head>
<body>
<div id="header">
	<img src="/resources/logo.jpg" atl="UnitBean">
	<p>Статьи, ${count}</p>
	<a href="/add">Добавить статью</a>
</div>
	<div id="main">
<c:forEach var="blog" items="${blogs}">
	<div class="blogpost" id="{$blog._id}">
		<a href="/post?id={$blog._id}"><h2>${blog.name}</h2></a>
		<img src="${blog.thumbnail}">
		<div class="blogContent">${blog.content}</div>
	</div>
</c:forEach>
	</div>
</body>
</html>
