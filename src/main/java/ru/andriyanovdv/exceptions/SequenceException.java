package ru.andriyanovdv.exceptions;

/**
 * Date: 26.03.2014
 * Time: 15:29
 * @author Ruslan Molchanov (ruslanys@gmail.com)
 *
 * Date: 09.11.2017
 * Time: 00:17
 * @author Daniil Andriyanov (8595dan@gmail.com)
 */
public class SequenceException extends RuntimeException {
    public SequenceException(String message) {
        super(message);
    }
}
