package ru.andriyanovdv.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import ru.andriyanovdv.model.Blog;

import java.util.List;

/**
 * Date: 26.03.2014
 * Time: 15:29
 * @author Ruslan Molchanov (ruslanys@gmail.com)
 *
 * Date: 09.11.2017
 * Time: 00:17
 * @author Daniil Andriyanov (8595dan@gmail.com)
 */
@Repository
public class BlogDao {
    @Autowired private MongoOperations mongoOperations;

    public void save(Blog blog) {
        mongoOperations.save(blog);
    }

    public Blog get(Long id) {
        return mongoOperations.findOne(Query.query(Criteria.where("id").is(id)), Blog.class);
    }

    public List<Blog> getAll() {
        return mongoOperations.findAll(Blog.class);
    }
    
    public void remove(Long id) {
        mongoOperations.remove(Query.query(Criteria.where("id").is(id)), Blog.class);
    }
}
