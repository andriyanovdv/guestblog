package ru.andriyanovdv.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Date: 26.03.2014
 * Time: 15:29
 * @author Ruslan Molchanov (ruslanys@gmail.com)
 *
 * Date: 09.11.2017
 * Time: 00:17
 * @author Daniil Andriyanov (8595dan@gmail.com)
 */
@Document(collection = Blog.COLLECTION_NAME)
public class Blog implements Serializable {
    public static final String COLLECTION_NAME = "blogposts";

    /**
     * В качестве ID может выступать UUID объекта из MongoDB (ObjectID).
     *
     * @Id private String id;
     */
    @Id
    private Long id;

    private String name;
    private String content;
    private String thumbnail;

    public Blog() {
    }

    public Blog(String name, String content, String thumbnail) {
        this.name = name;
        this.content = content;
        this.thumbnail = thumbnail;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return content;
    }

    public void setNumber(String content) {
        this.content = content;
    }

    public String getEmail() {
        return thumbnail;
    }

    public void setEmail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
