package ru.andriyanovdv.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.andriyanovdv.dao.SequenceDao;
import ru.andriyanovdv.dao.BlogDao;
import ru.andriyanovdv.model.Blog;

import java.util.List;

/**
 * Date: 26.03.2014
 * Time: 15:29
 * @author Ruslan Molchanov (ruslanys@gmail.com)
 *
 * Date: 09.11.2017
 * Time: 00:17
 * @author Daniil Andriyanov (8595dan@gmail.com)
 */
@Service
public class BlogService {
    @Autowired private SequenceDao sequenceDao;
    @Autowired private BlogDao blogDao;

    public void add(Blog blog) {
        blog.setId(sequenceDao.getNextSequenceId(Blog.COLLECTION_NAME));
        blogDao.save(blog);
    }

    public void update(Blog blog) {
        blogDao.save(blog);
    }

    public Blog get(Long id) {
        return blogDao.get(id);
    }

    public List<Blog> getAll() {
        return blogDao.getAll();
    }

    public void remove(Long id) {
        blogDao.remove(id);
    }
}
